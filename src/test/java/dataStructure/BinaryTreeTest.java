package dataStructure;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class BinaryTreeTest {
	
	@Test
	public void shouldInsertDataOnEmptyTree() {
		BinaryTree tree = new BinaryTree();
		
		tree.insert(10L);
		
		assertThat(tree.getRoot().getKey(), is(10L));
	}
	
	@Test
	public void shouldInsertSmallerValueOnLeft() {
		BinaryTree tree = new BinaryTree();
		
		tree.insert(39L);
		tree.insert(1L);
		
		assertThat(tree.getRoot().getLeft().getKey(), is(1L));
	}
	
	@Test
	public void shouldInsertSmallerValueOnRight() {
		BinaryTree tree = new BinaryTree();
		
		tree.insert(39L);
		tree.insert(789L);
		
		assertThat(tree.getRoot().getRight().getKey(), is(789L));
	}
	
	@Test
	public void shouldHaveSizeZeroWhenCreated() {
		BinaryTree tree = new BinaryTree();
		
		assertThat(tree.size(), is(0));
	}
	
	@Test
	public void shouldReturnTheNumberOfItemsInTheTree() {
		BinaryTree tree = new BinaryTree();
		
		tree.insert(9L);
		tree.insert(384L);
		tree.insert(-999L);
		tree.insert(8237283L);
		tree.insert(-8347839483L);
		
		assertThat(tree.size(), is(5));
	}
}