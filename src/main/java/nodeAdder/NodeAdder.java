package nodeAdder;

import dataGenerator.RandomNumberGenerator;
import dataStructure.BinaryTree;

import java.util.concurrent.locks.ReentrantLock;

public class NodeAdder implements Runnable {
	private volatile BinaryTree tree;
	private final ReentrantLock lock;
	
	NodeAdder(BinaryTree tree) {
		this.tree = tree;
		
		lock = new ReentrantLock();
	}
	
	@Override
	public void run() {
		Long number = RandomNumberGenerator.generate();
		tree.insert(number);
		System.out.println(number + " was added by thread " + Thread.currentThread().getId());
	}
}
