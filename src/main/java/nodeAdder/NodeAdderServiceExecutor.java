package nodeAdder;

import dataStructure.BinaryTree;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class NodeAdderServiceExecutor {
	
	private ScheduledExecutorService executor;
	private BinaryTree tree;
	
	public NodeAdderServiceExecutor(BinaryTree tree) {
		this.tree = tree;
		this.executor = new ScheduledThreadPoolExecutor(Runtime.getRuntime().availableProcessors());
	}
	
	public void addNodes() {
		NodeAdder nodeAdder = new NodeAdder(tree);
		executor.scheduleAtFixedRate(nodeAdder, 0, 1, TimeUnit.SECONDS);
	}
}
