package dataGenerator;

import java.util.Random;

public class RandomNumberGenerator {
	public static Long generate() {
		Random random = new Random();
		
		return random.nextLong();
	}
}
