package dataStructure;

public class BinaryTree {
	private volatile Node root;
	
	public void insert(Long value) {
		root = insert(root, value);
	}
	
	private  Node insert(Node node, Long value) {
		if (node == null) {
			root = new Node(value);
			
			return root;
		}
		
		if (value < node.getKey()) {
			node.setLeft(insert(node.getLeft(), value));
		} else {
			node.setRight(insert(node.getRight(), value));
		}
		
		return node;
	}
	
	protected Node getRoot() {
		return root;
	}
	
	public int size() {
		return countNodes(root);
	}
	
	private int countNodes(Node node) {
		if (node == null) return 0;
		return 1 + countNodes(node.getLeft()) + countNodes(node.getRight());
	}
}