package dataStructure;

public class Node {
	private Long key;
	private Node left;
	private Node right;
	
	Node(Long key) {
		this.key = key;
		left = null;
		right = null;
	}
	
	public Long getKey() {
		return key;
	}
	
	public Node getLeft() {
		return left;
	}
	
	public Node getRight() {
		return right;
	}
	
	public void setLeft(Node left) {
		this.left = left;
	}
	
	public void setRight(Node right) {
		this.right = right;
	}
}
