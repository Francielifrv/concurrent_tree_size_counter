import threadExecutor.ThreadExecutor;

public class Main {
	public static void main(String[] args) {
		ThreadExecutor threadExecutor = new ThreadExecutor();
		
		threadExecutor.execute();
	}
}
