package threadExecutor;

import counter.TreeNodeCounterServiceExecutor;
import dataStructure.BinaryTree;
import nodeAdder.NodeAdderServiceExecutor;

public class ThreadExecutor {
	private static final int NUMBER_OF_COUNTER_THREADS = 3;
	
	private volatile BinaryTree binaryTree = new BinaryTree();
	
	public void execute() {
		NodeAdderServiceExecutor nodeAdderServiceExecutor = new NodeAdderServiceExecutor(binaryTree);
		nodeAdderServiceExecutor.addNodes();
		
		TreeNodeCounterServiceExecutor counterServiceExecutor = new TreeNodeCounterServiceExecutor(NUMBER_OF_COUNTER_THREADS, binaryTree);
		counterServiceExecutor.countTreeNodes();
	}
}
