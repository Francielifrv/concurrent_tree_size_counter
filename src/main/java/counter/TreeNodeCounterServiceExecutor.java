package counter;

import dataStructure.BinaryTree;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class TreeNodeCounterServiceExecutor {
	private final int numberOfCounters;
	private final ScheduledExecutorService executor;
	private final BinaryTree tree;
	
	public TreeNodeCounterServiceExecutor(int numberOfCounters, BinaryTree tree) {
		this.numberOfCounters = numberOfCounters;
		this.executor = new ScheduledThreadPoolExecutor(Runtime.getRuntime().availableProcessors());
		this.tree = tree;
	}
	
	public synchronized void countTreeNodes() {
		List<Runnable> treeCounters = createCounters();
		
		treeCounters.forEach(task -> executor.scheduleAtFixedRate(task, 2, 1, TimeUnit.SECONDS));
	}
	
	private List<Runnable> createCounters() {
		List<Runnable> tasks = new ArrayList<>();
		
		for (int i = 0; i < numberOfCounters; i++) {
			tasks.add(new TreeNodeCounter(tree));
		}
		
		return tasks;
	}
	
}
