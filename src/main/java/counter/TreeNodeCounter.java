package counter;

import dataStructure.BinaryTree;

public class TreeNodeCounter implements Runnable {
	
	private BinaryTree tree;
	
	TreeNodeCounter(BinaryTree tree) {
		this.tree = tree;
	}
	
	@Override
	public void run() {
		System.out.println("Number of nodes: " + countNodes() + " on thread " + Thread.currentThread().getId());
	}
	
	private int countNodes() {
		return tree.size();
	}
}
